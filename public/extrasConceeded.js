const fs = require('fs');
const csvParser = require('csv-parse');
let startingMatch = 0;
let endingMatch = 0;

//Calculating match id range of matches played in 2016 

const parseMatches = csvParser({
    columns: true
}, function (err, matchData) {

    const arrayOfMatchData = [...matchData];

    endingMatch = arrayOfMatchData.reduce((accumulator, element) => {

        if (parseInt(element['season']) == '2016' && startingMatch == 0) {

            startingMatch = parseInt(element['id']);
            accumulator = parseInt(element['id']);
        } else if (parseInt(element['season']) == '2016') {
            accumulator++;
        }
        return accumulator;
    });

});

fs.createReadStream('./../Data/matches.csv').pipe(parseMatches);

//Calculating extras conceeded by each team played 

const parseDeliveries = csvParser({
    columns: true
}, function (err, deliveryData) {

    const arrayOfDeliveryData = [...deliveryData];
    let extrasOfTeams = {};
    let bowlingTeam = 'none';

    arrayOfDeliveryData.reduce((accumulator, element) => {

        let currentBowlingTeam = element['bowling_team'];

        if (parseInt(element['match_id']) >= startingMatch && parseInt(element['match_id']) <= endingMatch) {

            if (currentBowlingTeam == bowlingTeam) {
                accumulator += parseInt(element['extra_runs']);
            } else if (currentBowlingTeam !== bowlingTeam) {

                bowlingTeam = currentBowlingTeam;
                if (extrasOfTeams.hasOwnProperty(bowlingTeam)) {

                    extrasOfTeams[bowlingTeam] += accumulator;
                    accumulator = 0;
                } else if (extrasOfTeams.hasOwnProperty(bowlingTeam) == false) {

                    extrasOfTeams[bowlingTeam] = accumulator;
                    accumulator = 0;
                }
            }
        }
        return accumulator;
    }, 0);
    console.log(extrasOfTeams);
})
fs.createReadStream('./../Data/deliveries.csv').pipe(parseDeliveries);
