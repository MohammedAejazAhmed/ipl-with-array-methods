const fs = require('fs');
const csvParser = require('csv-parse');

const parseMatches = csvParser({
    columns: true
}, function (error, matchData) {

    const arrayOfMatchData = [...matchData];
    const winsPerTeam = {};
    let seasonsPlayedPerTeam = {};
    let teamsInThisSeason = {};
    let year = arrayOfMatchData[0]['season'];

    //Calculating total number of wins and seasons played by each team

    seasonsPlayedPerTeam = arrayOfMatchData.reduce((accumulator, element) => {

        //Incrementing total number of wins of current team

        if (winsPerTeam.hasOwnProperty(element['winner'])) {
            winsPerTeam[element['winner']] += 1;
        } else {
            winsPerTeam[element['winner']] = 1;
        }

        //Incrementing total number of seasons played by current team   

        const currentYear = element['season'];

        if (year == currentYear && teamsInThisSeason.hasOwnProperty(element['winner']) == false) {

            teamsInThisSeason[element['winner']] = 1;
            if (seasonsPlayedPerTeam.hasOwnProperty(element['winner'])) {
                seasonsPlayedPerTeam[element['winner']] += 1;
            } else {
                seasonsPlayedPerTeam[element['winner']] = 1;
            }
        } else if (year != currentYear) {
            year = currentYear;
            teamsInThisSeason = {};
        }

        return seasonsPlayedPerTeam;
    });

    //Calculating wins per team per season

    let winsPerSeason = [];
    const teamNames = Object.keys(winsPerTeam);
    const arrayOfWinsPerTeam = Object.values(winsPerTeam);
    const arrayOfSeasonsPlayedPerTeam = Object.values(seasonsPlayedPerTeam);

    winsPerSeason = arrayOfWinsPerTeam.map((element, index) => {

        element /= arrayOfSeasonsPlayedPerTeam[index];
        obj = {};
        obj[teamNames[index]] = element;
        return obj;
    });

    console.log(winsPerSeason);
});
fs.createReadStream('./../Data/matches.csv').pipe(parseMatches);
