const fs = require('fs');
const csvParser = require('csv-parse');
let startingMatch = 0;
let endingMatch = 0;

//Calculating match id range of matches played in 2015

const parseMatches = csvParser({
    columns: true
}, function (error, matchData) {

    const arrayOfMatchData = [...matchData];

    endingMatch = arrayOfMatchData.reduce((accumulator, element) => {

        if (parseInt(element['season']) == '2015' && startingMatch == 0) {
            startingMatch = parseInt(element['id']);
            accumulator = parseInt(element['id']);
        } else if (parseInt(element['season']) == '2015') {
            accumulator++;
        }
        return accumulator;
    });
});
fs.createReadStream('./../Data/matches.csv').pipe(parseMatches);


const parseDeliveries = csvParser({
    columns: true
}, function (error, deliveryData) {

    const arrayOfDeliveryData = [...deliveryData];
    let bowler = 'none';
    let ballsByBowler = {};
    let runsByBowler = {};

    //calculating total runs conceeded and balls bowled by each bowler    

    arrayOfDeliveryData.reduce((accumulator, element) => {

        let currentBowler = element['bowler'];

        if (parseInt(element['match_id']) >= startingMatch && parseInt(element['match_id']) <= endingMatch) {
            if (currentBowler == bowler) {
                accumulator += parseInt(element['total_runs']);
            } else if (currentBowler !== bowler) {
                if (bowler == 'none') {
                    accumulator = parseInt(element['total_runs']);
                } else if (runsByBowler.hasOwnProperty(bowler)) {

                    runsByBowler[bowler] += accumulator;
                    ballsByBowler[bowler] += 6;
                    accumulator = 0;
                } else if (runsByBowler.hasOwnProperty(element['bowler']) == false) {

                    runsByBowler[bowler] = accumulator;
                    ballsByBowler[bowler] = 6;
                    accumulator = 0;
                }
                bowler = element['bowler'];
            }
            return accumulator;
        }
    }, 0);

    //calculating economies of bowlers

    const arrayOfBallsByBowler = Object.values(ballsByBowler);
    const arrayOfRunsByBowler = Object.values(runsByBowler);
    const bowlerNames = Object.keys(runsByBowler);

    const economyByBowler = arrayOfRunsByBowler.map((element, index) => {

        const economy = element / arrayOfBallsByBowler[index] * 6;
        object = {};
        object[bowlerNames[index]] = economy;
        return object;
    });

    //sorting economies of bowlers

    const arrayOfEconomyByBowler = economyByBowler.map((element, index) => {
        
        const key = Object.keys(element);
        const value = Object.values(element);
        return [key, value];
    });

    arrayOfEconomyByBowler.sort((a, b) => {
        return a[1] - b[1];
    });

    console.log(arrayOfEconomyByBowler.filter((element, index) => {
        return index < 10;
    }));
});

fs.createReadStream('./../Data/deliveries.csv').pipe(parseDeliveries);
